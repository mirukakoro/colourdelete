### Ken Shibata / `colourdelete`

#### Contact

:envelope: [colourdelete@gmail.com](mailto:colourdelete@gmail.com)

:bird: [colourdelete](https://twitter.com/colourdelete)

GitLab.com: [colourdelete](https://gitlab.com/colourdelete)

GitHub.com: [colourdelete](https://github.com/colourdelete)


#### My GitHub Stats

[![My GitHub Stats](https://github-readme-stats.vercel.app/api?username=colourdelete&count_private=true)](https://github.com/anuraghazra/github-readme-stats)

<!--
**colourdelete/colourdelete** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
